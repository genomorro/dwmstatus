dwmstatus
=========

Deprecated: Now I use dstatus with some modifications. See: https://gitorious.org/dwm-stuff/dstatus/

This repo is still used with testing code

My own version of dwmstatus, it shows:

* Date
* Load averange
* Battery percentange and status (Discharging, Charging, Full, Unknown)
* Used Memory
* Volume in percentage and a small bar
* Removable media (mount point, total and used disk space)

These codes have heen very helpful:

* http://dwm.suckless.org/dwmstatus/
* https://github.com/Unia/dwmst
* http://www.lemoda.net/c/unix-regex/index.html

TODO:

* DONE Memory function should subtract cached and buffers memory but sysinfo cannot get cache memory.
* Is it use lots CPU?
