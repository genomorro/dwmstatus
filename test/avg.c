#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
  double avgs[3];
	
  if (getloadavg(avgs, 3) < 0) {
    perror("getloadavg");
    exit(1);
  }
  printf("%.2f %.2f %.2f\n", avgs[0], avgs[1], avgs[2]);

  return 0;
}
